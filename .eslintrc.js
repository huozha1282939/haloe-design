module.exports = {
  root: true,
  env: {
    node: true,
  },
  globals: {
    $: true,
    BMap: true,
  },
  extends: ["plugin:vue/vue3-essential"],
  parserOptions: {
    parser: "babel-eslint",
    ecmaVersion: 13,
    sourceType: "module",
  },
  rules: {
    "vue/script-setup-uses-vars": "off",
    "vue/html-indent": ["error", 2],
    'indent': ["error", 2],
    "no-console": "warn",
    "no-tabs": 0,
    "no-unused-vars": 0,
    "no-return-assign": 0,
    "vue/html-self-closing": "off",
    "vue/require-default-prop": 0,
    "vue/max-attributes-per-line": [
      // 配置成可允许一行attribute跟着标签
      "error",
      {
        singleline: 20,
        multiline: {
          max: 1,
          allowFirstLine: false,
        },
      },
    ],
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
  },
};
