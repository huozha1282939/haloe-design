#! /usr/bin/env node

const program = require('commander')

program
  .command('new <component> [chnName]')
  .description('create a new component')
  .action(() => {
    require('./create.js')()
  })

// 解析用户执行命令传入参数
program.parse(process.argv);
