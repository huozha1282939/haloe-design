/* eslint-disable no-console */
'use strict';

/**
 *  添加新组件自动化配置
 *  以button为例：make new button 按钮
 *  1、在 /src/components 目录下新建组件目录 /button，并完成目录结构的创建
 *  2、创建组件样式文件，/src/styles/components/button.less
 *  3、创建组件文档，/docs/button.md
 *  4、创建组件示例文件，/examples/routers/button.vue
 *  5、创建组件类型声明文件，/types/button.d.ts
 *  6、配置
 *      在 /src/components/index.js 文件中配置组件信息
 *      在 /src/styles/components/index.less 文件中引入该组件的样式文件
 *      将类型声明文件在 /types/haloe.components.d.ts 中引入
 *      /examples 中示例文件配置main.js 和 app.vue 需要自己手动修改
 */

const createComp = () => {
  process.on('exit', () => {
    console.log();
  });
  
  if (!process.argv[3]) {
    console.error('[组件名]必填 - Please enter new component name');
    process.exit(1);
  }
  
  const path = require('path');
  const fs = require('fs');
  const fileSave = require('file-save');
  const uppercamelcase = require('uppercamelcase');
  // 组件名称，比如 button
  const componentname = process.argv[3];
  // 组件的中文名称
  const chineseName = process.argv[4] || componentname;
  // 将组件名称转换为大驼峰形式， button => Button
  const ComponentName = uppercamelcase(componentname);
  // 组件包目录，/packages/button
  const PackagePath = path.resolve(__dirname, '../../src/components', componentname);
  
  const componentsIndexPath = path.join(__dirname, '../../src/components/index.js');
  
  // 需要添加的文件列表和文件内容的基本结构
  const NewFiles = [
    // /src/components/button/index.js
    {
      filename: 'index.js',
      // 文件内容，引入并导出组件
      content: `import ${ComponentName} from './${componentname}.vue';

export default ${ComponentName};`
    },
    // 定义组件的基本结构，/src/components/button/button.vue
    {
      filename: `${componentname}.vue`,
      // 文件内容
      content: `<template>
  <div>This is ${ComponentName}</div>
</template>

<script>
export default {
  name: '${ComponentName}'
};
</script>`
    },
    // 组件样式文件，/src/styles/components/button.less
    {
      filename: path.join('../../styles/components', `${componentname}.less`),
      // 文件基本结构
      content: `@${componentname}-prefix-cls: ~"@{css-prefix}${componentname}";

.@{${componentname}-prefix-cls} {
}`
    },
    // 组件文档，/docs/button.md，并设置文件标题
    {
      filename: path.join('../../../docs', `${componentname}.md`),
      content: `## ${chineseName}
### API
<br>`
    },
    // 定义组件示例文件，/examples/routers/button.vue
    {
      filename: path.join('../../../examples/routers', `${componentname}.vue`),
      // 文件内容，sfc
      content: `<template>
  <div>This is ${ComponentName}</div>
</template>

<script>
export default {

};
</script>`
    },
    // 组件类型声明文件， /types/button.d.ts
    {
      filename: path.join('../../../types', `${componentname}.d.ts`),
      // 类型声明文件基本结构
      content: `import type { DefineComponent } from 'vue';

export declare const ${ComponentName}: DefineComponent<{
}>`
    }
  ];
  
  // 判断组件是否已存在，如果已存在则直接退出
  const componentsFile = `${fs.readFileSync(componentsIndexPath)}`;
  if (componentsFile.indexOf(`{ default as ${ComponentName} }`) > -1) {
    console.error(`组件 ${componentname} 已存在.`);
    process.exit(1);
  }
  
  // 将组件添加到 /src/components/index.js
  const componentsImportText = `${fs.readFileSync(componentsIndexPath)}export { default as ${ComponentName} } from './${componentname}';`;
  fileSave(componentsIndexPath)
    .write(componentsImportText, 'utf8')
    .end('\n');
  
  // 将组件样式文件在 /src/styles/components/index.less 中引入
  const lessPath = path.join(__dirname, '../../src/styles/components/index.less');
  const lessImportText = `${fs.readFileSync(lessPath)}@import "./${componentname}.less";`;
  fileSave(lessPath)
    .write(lessImportText, 'utf8')
    .end('\n');
  
  // 将组件的类型声明文件在 /types/haloe.components.d.ts 中引入
  const elementTsPath = path.join(__dirname, '../../types/haloe.components.d.ts');
  
  let elementTsText = `${fs.readFileSync(elementTsPath)}`;
  
  const insertIndex = elementTsText.indexOf('interface') - 1;
  const importString = `export { ${ComponentName} } from './${componentname}'`;
  
  elementTsText = elementTsText.slice(0, insertIndex) + importString + '\n' + elementTsText.slice(insertIndex);
  
  fileSave(elementTsPath)
    .write(elementTsText, 'utf8')
    .end('\n');
  
  // 遍历 NewFiles 数组，创建列出的所有文件并写入文件内容
  NewFiles.forEach(file => {
    fileSave(path.join(PackagePath, file.filename))
      .write(file.content, 'utf8')
      .end('\n');
  });
  
  console.log('DONE!');
  
}

module.exports = createComp