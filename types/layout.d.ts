import Vue, { VNode } from 'vue';

export declare class Layout extends Vue {
  /**
   * 	侧边栏是否收起，可使用 v-model 双向绑定数据。
   * @default false
   */
   modelValue?: boolean;
  /**
   * 宽度
   * @default 200
   */
  width?: number;
  /**
   * 是否可收起，设为false后，默认触发器会隐藏，且响应式布局不会触发
   * @default false
   */
  collapsible?: boolean;
  /**
   * 收缩宽度，设置为 0 会出现特殊 trigger
   * @default 64
   */
  'collapsed-width'?: number;
  /**
   * 隐藏默认触发器
   * @default false
   */
  'hide-trigger'?: boolean;
  /**
   * 是否默认收起，设置了collapsible后设置此属性侧边栏仍会收起。
   * @default false
   */
  'default-collapsed'?: boolean;
  /**
   * 展开-收起时的回调true/false
   */
  $emit(eventName: 'on-collapse', []): this;
  /**
   * slot插槽对象
   */
  $slots: {
    /**
     * 自定义触发器
     * @default
     */
    trigger: VNode[];
  };
  /**
   * methods, 改变Sider展开-收起状态。
   */
  toggleCollapse(): void;
}
