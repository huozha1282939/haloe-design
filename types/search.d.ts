import Vue, { VNode } from 'vue';

export declare class Search extends Vue {
  /**
   * 区分是回车键搜索样式还是按钮搜索样式， 默认是按钮搜索样式
   * @default false
   */
  'is-enter': boolean;

  /**
   * 默认文字
   * @default '''
   */
  placeholder: string;

  /**
   * 过滤条件的选项，传入此值启用过滤条件样式
   * @default null
   */
  'filter-items': [];

  /**
   * 是否支持搜索
   * @default false
   */
  filterable: boolean;

  /**
   * 过滤条件选择器的宽度
   * @default 90
   */
  'filter-width': string | number;

  /**
   * 是否显示清空按钮
   * @default true
   */
  clearable: boolean;

  /**
   * 筛选条件默认值
   * @default null
   */
  'filter-value': any;

  /**
   * 点击搜索图标触发
   */
  $emit(eventName: 'on-submit'): this;

  /**
   * 筛选条件输入框切换时触发
   */
  $emit(eventName: 'on-select'): this;

  /**
   * 按下回车按钮触发
   */
  $emit(eventName: 'on-enter'): this;
}
