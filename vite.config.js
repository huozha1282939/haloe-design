import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import { resolve } from 'path'

const path = require("path");

const config = defineConfig({
  plugins: [vue(), vueJsx()],
  target: "es2015",
  alias: {
    "@": resolve(__dirname, 'src'), // 路径别名
  },
  build: {
    outDir: path.resolve(__dirname, "./dist"),
    lib: {
      entry: path.resolve(__dirname, "./src/index.js"),
      name: "HaloE-Design",
    },
    rollupOptions: {
      context: "globalThis",
      preserveEntrySignatures: "strict",
      external: ["vue"],
      output: [
        {
          format: "umd",
          exports: "named",
          sourcemap: false,
          entryFileNames: "haloe.min.js",
          chunkFileNames: "[name].js",
          assetFileNames: "[name].[ext]",
          namespaceToStringTag: true,
          inlineDynamicImports: false,
          manualChunks: undefined,
          globals: { vue: "Vue" },
        },
        {
          format: "es",
          exports: "named",
          sourcemap: false,
          entryFileNames: "haloe.min.esm.js",
          chunkFileNames: "[name].js",
          assetFileNames: "[name].[ext]",
          namespaceToStringTag: true,
          inlineDynamicImports: false,
          manualChunks: undefined,
          globals: { vue: "Vue" },
        },
      ],
    },
  },
  resolve: {
    extensions: [".mjs", ".js", ".ts", ".jsx", ".tsx", ".json", ".vue"],
  },
  css: {
    preprocessorOptions: {
      less:{
        charset: false,
        additionalData: `@use "${path.resolve(__dirname, 'src/styles/index.less')}"`
      }
    }
  }
});

export default config;
