## 时间轴
### API
<br>

* **Timeline Props**

|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|mode|时间轴方向，可选值为 horizontal（水平） 和 vertical（垂直）|String|vertical|
|pending|指定是否最后一个节点为幽灵节点|Boolean|false|


* **TimelineItem Props**

|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|color|圆圈颜色，可选值为blue、red、green，或自定义色值|String|blue|


* **TimelineItem slot**

|方法名|说明|
|------- |:------:|
|dot|自定义时间轴点内容|
|default|基本内容|


