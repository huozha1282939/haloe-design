### API
<br>

* **Alert Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|type|警告提示样式，可选值为info、success、warning、error|String|info|
|closable|是否可关闭|Boolean|false|
|show-icon|是否显示图标|Boolean|true|
|fade|是否应用动画，动画时长可能会引起占位的闪烁|Boolean|true|

* **Alert events**

|事件名    |说明    |返回值    |
|-------|:------:|:------:|
|on-close|关闭时触发|event|

* **Alert Slots**

|名称    |说明    |
|-------|:------:|
|无|警告提示内容|
|desc|警告提示辅助性文字介绍|
|icon|自定义图标内容|
|close|自定义关闭内容|