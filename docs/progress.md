
### API
<br>

* **Progress Props**

| 属性              |                      说明                      |         类型          |   默认值   |
|-----------------|:--------------------------------------------:|:-------------------:|:-------:|
| type            |      进度条类型，可选值为 `default`,`small`       |       String        | default |
| vertical        |                 进度条是否在垂直方向显示                 |       Boolean       |  false  |
| stroke-width    |                 进度条的宽度，单位px                  |       Number        |   10    |
| stroke-color    |            进度条的颜色，如传入数组，颜色显示为渐变色             | String &#124; Array |    -    |
| percent         |                    进度条百分比                    |       Number        |    0    |
| success-percent |                   进度条成功百分比                   |       Number        |    0    |
| status          | 进度条状态 `normal`, `active`, `wrong`, `success` |       String        | normal  |
| hide-info       |                 是否隐藏百分比数值或图标                 |       Boolean       |  false  |
| text-inside     |               进度条百分比是否显示在进度条内                |       Boolean       |  false  |



* **Progress Slots**

| 插槽名     |说明    |
|---------|:------:|
| default |默认插槽，自定义百分比显示内容|
