
### API
<br>

* **Input props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|type|输入框类型，可选值为 text、password 或 textarea|String|text|
|value|绑定的值，可使用 v-model 双向绑定|String|Number|空|
|size|输入框尺寸，可选值为large和small或者不设置|String|-|
|placeholder|占位文本|String|-|
|disabled|设置输入框为禁用状态|	Boolean|false|
|readonly|设置输入框为只读|	Boolean|false|
|maxlength|最大输入长度|Number|-|
|icon|输入框尾部图标，仅在 text 类型下有效|	String|-|
|rows|文本域默认行数，仅在 textarea 类型下有效|	Number|2|
|autosize|自适应内容高度，仅在 textarea 类型下有效，可传入对象，如 { minRows: 2, maxRows: 6 }|Boolean|Object|false|
|number|将用户的输入转换为 Number 类型|Boolean|false|


* **Input events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-enter|按下回车键时触发|无|
|on-click|设置 icon 属性后，点击图标时触发|无|
|on-change|数据改变时触发|event|
|on-focus|输入框聚焦时触发|无|
|on-blur|输入框失去焦点时触发|无|

* **Input slot**

|属性    |说明    |
|------- |:------:|:------:|--------:|
|prepend|前置内容，仅在 text 类型下有效|
|append|后置内容，仅在 text 类型下有效|



