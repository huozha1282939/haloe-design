## drawer
### API
* Drawer Props

| 属性          | 说明                                                         | 类型             | 默认值 |
| ------------- | ------------------------------------------------------------ | ---------------- | ------ |
| value         | 抽屉是否显示，可使用 v-model 双向绑定数据                    | Boolean          | false  |
| title         | 抽屉标题，如果使用 slot 自定义了页头，则 title 无效          | String           | -      |
| width         | 抽屉宽度，左、右方向时可用。当其值不大于 100 时以百分比显示，大于 100 时为像素 | Number\|String   | 256    |
| height        | 抽屉高度，上、下方向时可用。当其值不大于 100 时以百分比显示，大于 100 时为像素 | Number \| String | 256    |
| closable      | 是否显示右上角的关闭按钮                                     | Boolean          | true   |
| mask-closable | 是否允许点击遮罩层关闭                                       | Boolean          | true   |
| mask          | 是否显示遮罩层                                               | Boolean          | true   |
| mask-style    | 遮罩层样式                                                   | Object           | -      |
| styles        | 抽屉中间层的样式                                             | Object           | -      |
| scrollable    | 页面是否可以滚动                                             | Boolean          | false  |
| placement     | 抽屉的方向，可选值为 left、right、top、bottom                | String           | right  |
| transfer      | 是否将抽屉放置于 body 内                                     | Boolean          | true   |
| class-name    | 设置抽屉容器.haloe-drawer-wrap的类名                         | String           | -      |
| inner         | 是否设置抽屉在某个元素内打开，开启此属性时，应当关闭 transfer 属性 | Boolean          | false  |
| draggable     | 是否开启拖拽调整宽度                                         | Boolean          | false  |
| before-close  | 返回 Promise 可以阻止关闭                                    | Function         | -      |
| lock-scroll   | 是否禁止对页面滚动条的修改                                   | Boolean          | false  |

* Drawer methods

| 事件名称          | 说明                   | 返回值       |
| ----------------- | ---------------------- | ------------ |
| on-close          | 关闭抽屉时触发         | -            |
| on-visible-change | 显示状态发生变化时触发 | true / false |
| on-resize-width   | 调整宽度时触发         | width        |



* Drawer slots

| 名称    | 说明                 |
| ------- | -------------------- |
| header  | 自定义标题栏         |
| close   | 自定义右上角关闭内容 |
| trigger | 自定义调整宽度节点   |
| 默认    | 抽屉主体内容         |

