### API
<br>

* **TimePicker  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|type|显示类型，可选值为 time、timerange|String |time|
|model-value|日期，可以是 JavaScript 的 Date，例如 new Date() ，也可以是标准的日期格式，点击右边查看，注意：model-value 使用 v-model 时，值是 Date 类型，可以配合 @on-change 使用|Date|-|
|format|展示的时间格式	|Date|HH:mm:ss|
|steps|下拉列表的时间间隔，数组的三项分别对应小时、分钟、秒。例如设置为 [1, 15] 时，分钟会显示：00、15、30、45。	|Array|[]|
|placeholder|占位文本	|String|空|
|open|手动控制时间选择器的显示状态，true 为显示，false 为收起。使用该属性后，选择器不会主动关闭。建议配合 slot 及 confirm 和相关事件一起使用|Boolean|null|
|disabled|是否禁用选择器	|Boolean|false|
|clearable|是否显示清除按钮|Boolean|true|
|readonly|完全只读，开启后不会弹出选择器，只在没有设置 open 属性下生效|Boolean|false|
|editable|文本框是否可以输入，只在没有使用 slot 时有效|Boolean|false|
|disabled-hours|不可选时|Array|[]|
|disabled-minutes|不可选分钟|Array|[]|
|disabled-seconds|不可选秒|Array|[]|
|transfer|是否将弹层放置于 body 内，在 Tabs、带有 fixed 的 Table 列内使用时，建议添加此属性，它将不受父级样式影响，从而达到更好的效果	|Boolean|false|
|element-id|给表单元素设置 id，详见 Form 用法。|String|-|
|separator|两个时间的分隔符|String|-|
|capture|是否开启 capture 模式，也可通过全局配置	|Boolean|true|
|transfer-class-name|开启 transfer 时，给浮层添加额外的 class 名称	|Boolean|true|
|events-enabled|是否开启 Popper 的 eventsEnabled 属性，开启可能会牺牲一定的性能|Boolean|true|

* **TimePicker events**

|属性    |说明    |返回值    |
|-------|:------:|:------:|
|on-change|时间发生变化时触发|已经格式化后的时间，比如 09:41:00|
|on-open-change	|弹出浮层和关闭浮层时触发 |true/false|
|on-clickoutside	|点击外部关闭下拉菜单时触发 |event|

* **TimePicker Slots**

|名称    |说明    |
|-------|:------:|
|无	|自定义选择器的显示内容，建议与 open 等参数一起使用|
